﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    public enum Types
    {
        Harten,
        Klaveren,
        Schoppen,
        Ruiten
    }

    class Card
    {
        public static String[] CardNames = {"Een","Twee","Drie","Vier","Vijf","Zes","Zeven","Acht","Negen","Koning","Koning","Boer","As"};
        public Types type { get; set; }
        public string name { get; set; }
        public int value { get; set; }
        public char caracter { get; set; }

        public Card(Types CardType, string CardName)
        {
            this.type = CardType;
            this.name = CardName;
            this.load();
        }

        private void load()
        {
            switch (this.type)
            {
                case Types.Harten:
                    this.caracter = '♥';
                    break;
                case Types.Schoppen:
                    this.caracter = '♠';
                    break;
                case Types.Ruiten:
                    this.caracter = '♦';
                    break;
                case Types.Klaveren:
                    this.caracter = '♣';
                    break;
            }

            switch (this.name)
            {
                case "Koning":
                case "Konining":
                case "Boer":
                    this.value = 10;
                    break;
                case "As":
                    this.value = 11;
                    break;
                default:
                    this.value = Array.IndexOf(CardNames, this.name) + 1;
                    break;
            }
        }

        public void PrintCard()
        {
            Console.WriteLine(this.caracter + " " + this.name);
        }

    }
}
