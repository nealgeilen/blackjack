﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    class Player
    {
        public List<Card> Cards;

        public Player(Deck CardDeck)
        {
            this.Cards = CardDeck.GiveCardHand();
        }

        public int GetValueCards()
        {
            int value = 0;
            foreach(Card card in this.Cards)
            {
                value += card.value;
            }
            return value;
        }

        public void OvervieuwCards()
        {
            Console.WriteLine("Speler kaarten (" + this.GetValueCards() + "):");
            foreach (Card card in this.Cards)
            {
                card.PrintCard();
            }
            Console.WriteLine("");
        }


    }
}
