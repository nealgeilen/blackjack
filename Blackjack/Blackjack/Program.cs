﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            StartGame();
        }


        static public void StartGame()
        {
            Deck CardDeck = new Deck();
            Player player = new Player(CardDeck);
            Dealer dealer = new Dealer(CardDeck);
            StartRound(CardDeck,player,dealer);
        }

        static public void StartRound(Deck CardDeck,Player player, Dealer dealer)
        {
            string command;
            bool done = true;
            bool exit = true;
            dealer.OvervieuwCards();
            player.OvervieuwCards();
            do
            {
                Console.Write("Wat is de volgende actie (of ? voor help): ");
                command = Console.ReadLine();
                if (command != "")
                {
                    Console.Clear();
                    switch (command)
                    {
                        case "?":
                            Console.WriteLine("Acties zijn: 'HIT', 'STAND', 'OVERVIEW'");
                            break;
                        case "HIT":
                            player.Cards.Add(CardDeck.GiveCard());
                            dealer.RevealCard();
                            dealer.hiddenCards.Add(CardDeck.GiveCard());
                            break;
                        case "STAND":
                            dealer.RevealCard();
                            dealer.hiddenCards.Add(CardDeck.GiveCard());
                            break;
                        case "EXIT":
                            exit = false;
                            break;
                        default:
                            break;
                    }

                    dealer.OvervieuwCards();
                    player.OvervieuwCards();

                    if (dealer.GetValueCards() > 21 && !(player.GetValueCards() > 21))
                    {
                        Console.WriteLine("You won, Dealer has more then 21!");
                        done = false;
                    }
                    if (player.GetValueCards() > 21)
                    {
                        Console.WriteLine("You have lost, You have more then 21");
                        done = false;
                    }




                }
            } while (exit);
            Console.WriteLine("");
        }
    }

}
