﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    class Deck
    {

        public List<Card> CardDeck = new List<Card>();

        public Deck()
        {
            if (this.CardDeck.Count != (Enum.GetNames(typeof(Types)).Length * Card.CardNames.Length))
            {
                this.CreateCards();
            }
            this.Shuffle();
        }

        public List<Card> CreateCards()
        {
            List<Card> Deck = new List<Card>();

            for (int x = 0; x < Card.CardNames.Length; x++)
            {
                for (int i = 0; i < Enum.GetNames(typeof(Types)).Length; i++)
                {
                    Deck.Add(new Card((Types)i,Card.CardNames[x]));
                }
            }
            this.CardDeck = Deck;
            return Deck;
        }

        public Card GiveCard()
        {
            Card card = this.CardDeck[0];
            this.CardDeck.Remove(card);
            return card;
        }

        public List<Card> GiveCardHand()
        {
            return new List<Card>() { this.GiveCard(), this.GiveCard()};
        }


        public void Shuffle()
        {
            Random rng = new Random();
            int n = this.CardDeck.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                Card value = this.CardDeck[k];
                this.CardDeck[k] = this.CardDeck[n];
                this.CardDeck[n] = value;
            }
        }

    }
}
