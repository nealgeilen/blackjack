﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    class Dealer
    {
        private Deck CardDeck;
        public List<Card> visiableCards = new List<Card>();
        public List<Card> hiddenCards = new List<Card>();
        public Dealer(Deck CardDeck)
        {
            this.CardDeck = CardDeck;
            this.visiableCards = CardDeck.GiveCardHand();
        }

        public void RevealCard()
        {
            if(hiddenCards.Count > 0)
            {
                Card card = hiddenCards[0];
                this.visiableCards.Add(card);
                hiddenCards.Remove(card);
            } else
            {
                this.hiddenCards.Add(this.CardDeck.GiveCard());
                this.RevealCard();
            }
        }

        public void OvervieuwCards()
        {
            Console.WriteLine("Dealer's kaarten (" + this.GetValueCards() + "):");
            foreach (Card card in visiableCards)
            {
                card.PrintCard();
            }
            for (int i = 0; i < this.hiddenCards.Count; i++)
            {
                Console.WriteLine("<hidden>");
            }
            Console.WriteLine();
        }

        public int GetValueCards()
        {
            int value = 0;
            foreach (Card card in this.visiableCards)
            {
                value += card.value;
            }
            return value;
        }




    }
}
